import React from "react";

const daysOfWeek = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
  "sunday",
];

export const DayOfTheWeek = () => {
  return (
    <>
      {daysOfWeek.map((day) => {
        return <p>{`Day of week: ${day.charAt(0).toUpperCase()}${day.slice(1)}`}</p>
      })}
    </>
  );
};

export const DayOfTheWeekAlt = () => {
  return (
    <>
      {daysOfWeek.map((day) => (
        <p>{`Day of week: ${day.charAt(0).toUpperCase()}${day.slice(1)}`}</p>
      ))}
    </>
  );
};
